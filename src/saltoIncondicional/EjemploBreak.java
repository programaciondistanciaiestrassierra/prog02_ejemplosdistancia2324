/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package saltoIncondicional;

/**
 *
 * @author luisnavarro
 */
public class EjemploBreak {
    

     public static void main(String[] args) {
        // Declaración de variables
            int contador;
           
        //Procesamiento y salida de información     
       /* Este bucle sólo se ejecutará en 6 ocasiones, ya que cuando
        * la variable contador sea igual a 7 encontraremos un break que
        * romperá el flujo del bucle, transfiriéndonos el control a la 
        * sentencia que imprime el  mensaje  de Fin del programa.
        */
        for (contador=1;contador<=10;contador++){
            if (contador==7)
                 break;
            System.out.println ("Valor: " + contador); // Es una forma muy inapropiada de salir del bucle!!      
        }
        System.out.println ("Fin del programa");   
     }  
}