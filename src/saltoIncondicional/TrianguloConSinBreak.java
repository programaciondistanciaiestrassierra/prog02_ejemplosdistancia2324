/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package saltoIncondicional;

/**
 *
 * @author luisnavarro
 */
public class TrianguloConSinBreak {
  
    public static void main(String[] args)
    {        
        String cad="";
        for (int i=0;i<5;i++)
        {
            cad="";
            for (int j=0;j<5;j++) {
                cad=cad+"*";
                if (i==j) break;
            }
            System.out.println(cad);
        }   
        
        //
        for (int i=0;i<5;i++)
        {
            cad="";
            for (int j=0;j<=i;j++) {
                cad=cad+"*";
            }
            System.out.println(cad);
        }       
    }
}