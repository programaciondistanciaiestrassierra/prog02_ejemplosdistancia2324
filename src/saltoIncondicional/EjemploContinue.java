/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package saltoIncondicional;

/**
 *
 * @author luisnavarro
 */
public class EjemploContinue {
    

     public static void main(String[] args) {
        // Declaración de variables
          int contador;
        //Procesamiento y salida de información    
		  System.out.println ("Imprimiendo los números pares que hay del 1 al 10... "); 
          for (contador=1;contador<=10;contador++){
            if (contador % 2 != 0) 
			   continue;
            System.out.print(contador + " ");         
          }
          System.out.println ("\nFin del programa");   
        /* Las iteraciones del bucle que generarán la impresión de cada uno de los números
         * pares, serán aquellas en las que el resultado de calcular el resto de la división
         * entre 2 de cada valor de la variable contador, sea igual a 0.
         */
    }
}