/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package saltoIncondicional;

/**
 *
 * @author luisnavarro
 */
public class EjemploEtiqueta {
    

     public static void main(String[] args) {
	    /*Creamos cabecera del bucle*/
        for (int i=1; i<3; i++){
            bloqueUno:{   //Creamos primera etiqueta
                bloqueDos:{    //Creamos segunda etiqueta
                    System.out.println("Iteración: "+i);
                    if (i==1) break bloqueUno;   //Llevamos a cabo el primer salto
                    if (i==2) break bloqueDos;   //Llevamos a cabo el segundo salto           
                }
                System.out.println("después del bloque dos");
            }
            System.out.println("después del bloque uno");
        }
        System.out.println("Fin del bucle  for");
    }
}