/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bucles;

/**
 *
 * @author atecaiestrassierra
 */
public class NumeroCifrasFor {

    public static void main(String[] args) {
        int numero = 3456;
        int residuo = numero;
        int numCifras = 0;
        //supongo residuo>0
        //cada vez q lo divida entre 10 y no llegue a 0 es una cifra más
        while (residuo > 0) {
            residuo /= 10;
            numCifras++;
        }
        System.out.println("num cifras=" + numCifras);
         numCifras = 0;
        for (int residuo2 = numero; residuo2 > 0; residuo2 /= 10, numCifras++);

        System.out.println("num cifras=" + numCifras);
    }

}
