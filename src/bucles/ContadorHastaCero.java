/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bucles;

import java.util.Scanner;

/**
 *
 * @author luisnavarro
 */
public class ContadorHastaCero {

    public static void main(String[] args) {
        // Declaración de variables
        Scanner teclado = new Scanner(System.in);
        final int CODIGO = -1; // Constante que contiene el código correcto
        double notaIntroducida;  // Código introducio por el usuario
        int numNotas = 0;      // Contador que representa el número de intentos
        double sumaNotas=0;
// Entrada de datos
        do {
            System.out.print("Introduzca nota (para finalizar meta "+CODIGO+" ): ");
            notaIntroducida = teclado.nextDouble();
            
            //sumaNotas+=notaIntroducida;
            if (notaIntroducida>=0) 
            {
                numNotas=numNotas+1;
                sumaNotas+=notaIntroducida;
            }
        } while ((notaIntroducida != CODIGO));// && (numNotas < 3)); // Mientras el código sea incorrecto y no hayamos llegado al límite de intentos
        //numNotas--;
        //sumaNotas--;
// Comprobación de código correcto
        if (notaIntroducida == CODIGO) {
            System.out.println("Ha introducido "+numNotas+" notas.");
            System.out.println("Y la media es: "+sumaNotas/numNotas);
        } else {
            System.out.println("Número de intentos superado. Acceso bloqueado");
        }

    }
}
